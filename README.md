# Data_Mining_Hyperpartisan_News_Detection

Data mining group Project for Fall 2018

1. The project requires 3.4+ . Please check the python version with 
	python --version

2. Create a virtual environment folder with the following command.
	python -m venv hyperpartisan-env

3. For Windows use run the following command
	hyperpartisan-env\Scripts\activate.bat

4. For Unix run 
	source hyperpartisan-env/bin/activate

5. Install required dependencies with the following command
	pip install -r requirements.txt --progress-bar=off

6. Run the code using
	cd src
	python model_training_combine_features.py

7. The terminal after running the above mentioned file will prompt 
for usage of cross validation. Input 'y' to do cross validation. After
that a prompt is given for selecting classifiers. Input '5' to run for
all classifiers used.

8. The accuracy values are outputted to the terminal while the ROC curves
are generated in the Output folder.