from textstat import textstat


class Article:
    article_dict = {}

    def __init__(self, article_dict):
        self.article_dict = article_dict

    def main_text(self):
        return self.article_dict.get('mainText', '')

    def author(self):
        return self.article_dict['author']

    def hyperlinks(self):
        return self.article_dict.get('hyperlink', None)

    def orientation(self):
        return self.article_dict['orientation']

    def is_hyper_partisan(self):
        if self.orientation() == 'mainstream':
            return False
        else:
            return True

    def paragraphs(self):
        return self.article_dict['paragraph']

    def paragraph_count(self):
        return len(self.paragraphs())

    def paragraph_avg_length(self):
        if self.paragraph_count() > 0:
            return float(len(self.main_text()) / self.paragraph_count())
        else:
            return 0

    def portal(self):
        return self.article_dict['portal']

    def quotes(self):
        return self.article_dict.get('quote', None)

    def title(self):
        return self.article_dict['title']

    def uri(self):
        return self.article_dict['uri']

    def veracity(self):
        return self.article_dict['veracity']

    def readability_score(self):
        return textstat.flesch_reading_ease(self.main_text())

    def link_count(self):
        if self.hyperlinks():
            return len(self.hyperlinks())
        else:
            return 0

    def quote_avg_length(self):
        avg_quote_length = 0.0
        total_quote_length = 0.0
        if self.quotes():
            for quote in self.quotes():
                quote_start = quote['start']
                quote_end = quote['end']
                total_quote_length += (quote_end - quote_start)
            avg_quote_length = total_quote_length / len(self.quotes())
        return avg_quote_length
