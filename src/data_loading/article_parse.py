from pathlib import Path
import glob
from data_loading.article import Article
import xmlschema


class ArticleParser:
    articles = []
    main_texts = []
    labels = []
    para_counts = []
    para_avg_length = []
    readability_scores = []
    link_amount = []
    quote_avg_length = []

    def __init__(self):
        self.script_directory = Path(__file__).resolve().parent
        self.data_set_dir = Path(self.script_directory.parent.parent / 'Resources/Dataset/')
        self.article_data_dir = Path(self.data_set_dir / 'articles')
        self.articles = self.get_all_articles()

    def get_all_articles(self):
        article_schema = xmlschema.XMLSchema(f'{self.data_set_dir}/schema.xsd')
        article_file_paths = glob.glob(f'{self.article_data_dir}/*.xml')
        article_objects = []
        for xml_doc_path in article_file_paths:
            xml_content = article_schema.to_dict(xml_doc_path)
            article_object = Article(xml_content)
            if article_object.main_text():
                article_objects.append(article_object)
        return article_objects

    def get_main_texts(self):
        if len(self.main_texts) == 0:
            for article in self.articles:
                self.main_texts.append(article.main_text())
        return self.main_texts

    def get_labels(self):
        if len(self.labels) == 0:
            for article in self.articles:
                self.labels.append(article.is_hyper_partisan())
        return self.labels

    def get_para_counts(self):
        if len(self.para_counts) == 0:
            for article in self.articles:
                self.para_counts.append(article.paragraph_count())
        return self.para_counts

    def get_para_avg_lengths(self):
        if len(self.para_avg_length) == 0:
            for article in self.articles:
                self.para_avg_length.append(article.paragraph_avg_length())
        return self.para_avg_length

    def get_readability_scores(self):
        if len(self.readability_scores) == 0:
            for article in self.articles:
                self.readability_scores.append(article.readability_score())
        return self.readability_scores

    def get_link_amounts(self):
        if len(self.link_amount) == 0:
            for article in self.articles:
                self.link_amount.append(article.link_count())
        return self.link_amount

    def get_avg_quote_lengths(self):
        if len(self.quote_avg_length) == 0:
            for article in self.articles:
                self.quote_avg_length.append(article.quote_avg_length())
        return self.quote_avg_length








