from sklearn.base import BaseEstimator, TransformerMixin
from textstat import textstat
from pandas import DataFrame
from sklearn.preprocessing import normalize
import numpy as np


class AverageParagraphLengthExtractor(BaseEstimator, TransformerMixin):
    def __init__(self):
        pass

    def transform(self, df, y=None):
        df_column = df['para_avg_length']
        np_array_from_df = np.array(df_column).ravel()
        np_array_from_df = np_array_from_df.reshape(np_array_from_df.shape[0], 1)
        return normalize(np_array_from_df)

    def fit(self, df, y=None):
        return self


class ParagraphCountExtractor(BaseEstimator, TransformerMixin):
    def __init__(self):
        pass

    def transform(self, df, y=None):
        df_column = df['para_count']
        np_array_from_df = np.array(df_column).ravel()
        np_array_from_df = np_array_from_df.reshape(np_array_from_df.shape[0], 1)
        return normalize(np_array_from_df)

    def fit(self, df, y=None):
        return self


class ReadabilityExtractor(BaseEstimator, TransformerMixin):
    def __init__(self):
        pass

    def readability_score(self, text):
        score = textstat.flesch_reading_ease(text)
        if score < 0:
            score = 0.0
        return score

    def transform(self, df, y=None):
        np_array_from_df = np.array(df['text'].apply(self.readability_score)).ravel()
        np_array_from_df = np_array_from_df.reshape(np_array_from_df.shape[0], 1)
        return normalize(np_array_from_df)

    def fit(self, df, y=None):
        return self


class LinkAmountExtractor(BaseEstimator, TransformerMixin):
    def __init__(self):
        pass

    def transform(self, df, y=None):
        df_column = df['link_count']
        np_array_from_df = np.array(df_column).ravel()
        np_array_from_df = np_array_from_df.reshape(np_array_from_df.shape[0], 1)
        return normalize(np_array_from_df)

    def fit(self, df, y=None):
        return self


class AverageQuoteLengthExtractor(BaseEstimator, TransformerMixin):
    def __init__(self):
        pass

    def transform(self, df, y=None):
        df_column = df['quote_avg_length']
        np_array_from_df = np.array(df_column).ravel()
        np_array_from_df = np_array_from_df.reshape(np_array_from_df.shape[0], 1)
        return normalize(np_array_from_df)

    def fit(self, df, y=None):
        return self


class ColumnExtractor(BaseEstimator, TransformerMixin):

    def __init__(self, column_name):
        self.column_name = column_name

    def transform(self, df, y=None):
        column = DataFrame(df[f'{self.column_name}'])
        return np.array(column).ravel()

    def fit(self, df, y=None):
        return self


class DummyExtractor(BaseEstimator, TransformerMixin):

    def __init__(self, name):
        self.name = name

    def transform(self, obj, y=None):
        print(f'{self.name}', 'features shape', obj.shape)
        return obj

    def fit(self, df, y=None):
        return self
