from sklearn import model_selection, preprocessing, naive_bayes, svm, linear_model, ensemble
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.metrics import roc_curve, auc
from pprint import pprint
from scipy import interp
from data_loading.article_parse import ArticleParser
from utils.model_utils import train_and_score
from feature_extraction.feature_extractors import *
import pandas
import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path

script_directory = Path(__file__).resolve().parent
print('Loading input data')
article_parser = ArticleParser()

inputDF = pandas.DataFrame()
inputDF['text'] = article_parser.get_main_texts()
inputDF['para_count'] = article_parser.get_para_counts()
inputDF['para_avg_length'] = article_parser.get_para_avg_lengths()
inputDF['link_count'] = article_parser.get_link_amounts()
inputDF['quote_avg_length'] = article_parser.get_avg_quote_lengths()

# label encode the target variable
encoder = preprocessing.LabelEncoder()
labels = encoder.fit_transform(np.array(article_parser.get_labels()))
inputDF['labels'] = labels
# split the dataset into training and validation datasets
# train_x, valid_x, train_y, valid_y = model_selection.train_test_split(inputDF, labels)


tfidf_word_ngram_pipeline = Pipeline([('extract', ColumnExtractor('text')),
                                      ('count', TfidfVectorizer(analyzer='word', token_pattern=r'\w{1,}',
                                                                ngram_range=(1, 3), max_features=5000))])
tfidf_char_ngram_pipeline = Pipeline([('extract', ColumnExtractor('text')),
                                      ('count', TfidfVectorizer(analyzer='char', token_pattern=r'\w{1,}',
                                                                ngram_range=(1, 3), max_features=5000))])


combine_feats_pipeline = Pipeline([
    ('feats', FeatureUnion([
        ('ngram_word', tfidf_word_ngram_pipeline),  # word level ngram 1-3
        ('ngram_char', tfidf_char_ngram_pipeline),  # char level ngram 1-3
        ('para_count', ParagraphCountExtractor()),  # paragraph count of articles
        ('para_avg_length', AverageParagraphLengthExtractor()),  # avg paragraph length
        ('read_score', ReadabilityExtractor()),  # readability score
        ('link_amount', LinkAmountExtractor()),  # link count in article
        ('quote_avg_length', AverageQuoteLengthExtractor())  # quote length avg
    ]))
])


def do_simple_train_test_split():
    print('Using simple train test split')
    train_x, valid_x, train_y, valid_y = model_selection.train_test_split(inputDF, inputDF['labels'])

    score, predict_proba = train_and_score(naive_bayes.MultinomialNB(), 'Naive Bayes',
                                           tfidf_word_ngram_pipeline, train_x, train_y, valid_x, valid_y)
    print('NB, WordLevel TF-IDF: pipeline score', score)

    score, predict_proba = train_and_score(naive_bayes.MultinomialNB(), 'Naive Bayed',
                                           combine_feats_pipeline, train_x, train_y, valid_x, valid_y)
    print('NB, combined pipeline', score)

    score, predict_proba = train_and_score(linear_model.LogisticRegression(solver='lbfgs', max_iter=1000),
                                           'Logistic Regression',
                                           combine_feats_pipeline, train_x, train_y, valid_x, valid_y)
    print('LR, combined pipeline', score)

    score, predict_proba = train_and_score(svm.SVC(kernel='rbf', C=630.0, gamma='auto'), 'SVM',
                                           combine_feats_pipeline, train_x, train_y, valid_x, valid_y)
    print('SVM(Gaussian), combined pipeline', score)

    score, predict_proba = train_and_score(ensemble.RandomForestClassifier(n_estimators=100), 'Random Forest',
                                           combine_feats_pipeline, train_x, train_y, valid_x, valid_y)
    print('Random Forest, combined pipeline', score)


def get_selected_classifier():
    classier_no = int(input('Select Classifier: \n1. NB\n2. LR\n3. SVM\n4. RF\n5. All\n'))
    classifiers = []
    classifier_names = []
    if classier_no == 1:
        print('Using Naive bayes')
        classifier_names.append('Naive Bayes')
        classifiers.append(naive_bayes.MultinomialNB())
        return classifier_names, classifiers
    elif classier_no == 2:
        print('Using LogisticRegression')
        classifier_names.append('Logistic Regression')
        classifiers.append(linear_model.LogisticRegression(solver='lbfgs', max_iter=1000))
        return classifier_names, classifiers
    elif classier_no == 3:
        print('Using SVM(Gaussian)')
        classifier_names.append('SVM')
        classifiers.append(svm.SVC(kernel='rbf', C=630.0, gamma='auto', probability=True))
        return classifier_names, classifiers
    elif classier_no == 4:
        print('Using Random Forest')
        classifier_names.append('Random Forest')
        classifiers.append(ensemble.RandomForestClassifier(n_estimators=100))
        return classifier_names, classifiers
    elif classier_no == 5:
        print('Using Naive Bayes, LR, SVM, Random Forest')
        classifier_names.append('Naive Bayes')
        classifier_names.append('Logistic Regression')
        classifier_names.append('SVM')
        classifier_names.append('Random Forest')

        classifiers.append(naive_bayes.MultinomialNB())
        classifiers.append(linear_model.LogisticRegression(solver='lbfgs', max_iter=1000))
        classifiers.append(svm.SVC(kernel='rbf', C=630.0, gamma='auto', probability=True))
        classifiers.append(ensemble.RandomForestClassifier(n_estimators=100))
    else:
        print('!!! no classier was selected using NB')
        return 'NB', classifiers


def do_k_fold_cross_validation():

    tprs = []
    aucs = []
    mean_fpr = np.linspace(0, 1, 100)
    i = 1
    classifier_combined_scores = []
    classifier_names, classifiers = get_selected_classifier()

    for idx in range(len(classifiers)):
        classifier_name = classifier_names[idx]
        classifier = classifiers[idx]
        print(f'Using k=5 fold cross validation for {classifier_name}')
        k_fold = model_selection.KFold(n_splits=5, shuffle=True, random_state=np.random.randint(0, 100))
        split_count = 0
        for train_index, test_index in k_fold.split(X=inputDF, y=inputDF['labels']):
            split_count += 1
            print('Current split', split_count)
            train_x = inputDF.iloc[train_index]
            train_y = train_x['labels']
            valid_x = inputDF.iloc[test_index]
            valid_y = valid_x['labels']

            score, predict_proba = train_and_score(classifier, classifier_name,
                                                   combine_feats_pipeline, train_x, train_y, valid_x, valid_y)
            classifier_combined_scores.append(score)
            print(f'Accuracy Score for split {split_count} is {score}')
            print('----------------------------------------------------------\n')
            # Compute ROC curve and area the curve
            fpr, tpr, thresholds = roc_curve(valid_y, predict_proba[:, 1])
            tprs.append(interp(mean_fpr, fpr, tpr))
            tprs[-1][0] = 0.0
            roc_auc = auc(fpr, tpr)
            aucs.append(roc_auc)
            plt.plot(fpr, tpr, lw=1, alpha=0.3,
                     label='ROC fold %d (AUC = %0.2f)' % (i, roc_auc))
            i += 1

        print(f'{classifier_name}, combined pipeline scores')
        pprint(classifier_combined_scores)
        print(f'{classifier_name}, combined pipeline avg score', np.mean(classifier_combined_scores))

        print(f'----------------{classifier_name} Complete---------------------------\n')

        plt.plot([0, 1], [0, 1], linestyle='--', lw=2, color='r',
                 label='Chance', alpha=.8)

        mean_tpr = np.mean(tprs, axis=0)
        mean_tpr[-1] = 1.0
        mean_auc = auc(mean_fpr, mean_tpr)
        std_auc = np.std(aucs)
        plt.plot(mean_fpr, mean_tpr, color='b',
                 label=r'Mean ROC (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
                 lw=2, alpha=.8)

        std_tpr = np.std(tprs, axis=0)
        tprs_upper = np.minimum(mean_tpr + std_tpr, 1)
        tprs_lower = np.maximum(mean_tpr - std_tpr, 0)
        plt.fill_between(mean_fpr, tprs_lower, tprs_upper, color='grey', alpha=.2,
                         label=r'$\pm$ 1 std. dev.')

        plt.xlim([-0.05, 1.05])
        plt.ylim([-0.05, 1.05])
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.title(f'{classifier_name} ROC')
        plt.legend(loc="lower right")
        plt.savefig(f'{script_directory}/../Output/{classifier_name}_ROC_curve.png')


do_k_fold = input('Use K fold cross validation? y/n ')
if do_k_fold == 'y':
    do_k_fold_cross_validation()
else:
    do_simple_train_test_split()

