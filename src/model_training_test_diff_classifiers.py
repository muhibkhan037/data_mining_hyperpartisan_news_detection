#!/usr/bin/env python
# coding: utf-8

# In[1]:


from sklearn import model_selection, preprocessing, naive_bayes, metrics
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn import linear_model
from sklearn import svm, ensemble


import pandas
from data_loading.article_parse import ArticleParser


article_parser = ArticleParser()
articles = article_parser.get_all_articles()
main_texts = article_parser.get_main_texts()
labels = article_parser.get_labels()

trainDF = pandas.DataFrame()
trainDF['text'] = main_texts
trainDF['label'] = labels

print(trainDF.shape)

# split the dataset into training and validation datasets
train_x, valid_x, train_y, valid_y = model_selection.train_test_split(trainDF['text'], trainDF['label'])


# label encode the target variable
encoder = preprocessing.LabelEncoder()
train_y = encoder.fit_transform(train_y)
valid_y = encoder.fit_transform(valid_y)


def train_model(classifier, feature_vector_train, label, feature_vector_valid, is_neural_net=False):
    # fit the training dataset on the classifier
    classifier.fit(feature_vector_train, label)

    # predict the labels on validation dataset
    predictions = classifier.predict(feature_vector_valid)

    if is_neural_net:
        predictions = predictions.argmax(axis=-1)
        
    # Add Accuracy as a attribute of Classifier
    classifier.accuracy = metrics.accuracy_score(predictions, valid_y)
    
    return classifier

# In[2]:


# -------------Count Vectors-----------------
# create a count vectorizer object
count_vect = CountVectorizer(analyzer='word', token_pattern=r'\w{1,}')
count_vect.fit(trainDF['text'])

# transform the training and validation data using count vectorizer object
xtrain_count = count_vect.transform(train_x)
xvalid_count = count_vect.transform(valid_x)


# CV_Naive_model = train_model(naive_bayes.MultinomialNB(), xtrain_count, train_y, xvalid_count)
# print("NB, Count Vectors: ", CV_Naive_model.accuracy)


# Naive Bayes on Count Vectors
CountV_Naive_model = train_model(naive_bayes.MultinomialNB(), xtrain_count, train_y, xvalid_count)
print("NB, Count Vectors: ", CountV_Naive_model.accuracy)

# Linear Classifier on Count Vectors
CountV_Linear_model = train_model(linear_model.LogisticRegression(), xtrain_count, train_y, xvalid_count)
print("LR, Count Vectors: ", CountV_Linear_model.accuracy)


# In[3]:


# -------------Word Level Vectors-----------------
# word level tf-idf
tfidf_vect = TfidfVectorizer(analyzer='word', token_pattern=r'\w{1,}', max_features=5000)
tfidf_vect.fit(trainDF['text'])
xtrain_tfidf = tfidf_vect.transform(train_x)
xvalid_tfidf = tfidf_vect.transform(valid_x)


# Naive Bayes on Word Level TF IDF Vectors
Word_Naive_model = train_model(naive_bayes.MultinomialNB(), xtrain_tfidf, train_y, xvalid_tfidf)
print("NB, WordLevel TF-IDF: ", Word_Naive_model.accuracy)

# Linear Classifier on Word Level TF IDF Vectors
Word_Linear_model = train_model(linear_model.LogisticRegression(), xtrain_tfidf, train_y, xvalid_tfidf)
print("LR, WordLevel TF-IDF: ", Word_Linear_model.accuracy)


# In[4]:


# ---------------ngram level tf-idf----------------

# Parameter to Change val #ngram_range : tuple (min_n, max_n):: 2,4
# The lower and upper boundary of the range of n-values for different n-grams to be extracted.
tfidf_vect_ngram = TfidfVectorizer(analyzer='word', token_pattern=r'\w{1,}', ngram_range=(2, 4), max_features=5000)
tfidf_vect_ngram.fit(trainDF['text'])
xtrain_tfidf_ngram = tfidf_vect_ngram.transform(train_x)
xvalid_tfidf_ngram = tfidf_vect_ngram.transform(valid_x)

# Naive Bayes on Ngram Level TF IDF Vectors
Ngram_Naive_model = train_model(naive_bayes.MultinomialNB(), xtrain_tfidf_ngram, train_y, xvalid_tfidf_ngram)
print ("NB, N-Gram Vectors: ", Ngram_Naive_model.accuracy)

# Linear Classifier on Ngram Level TF IDF Vectors
Ngram_Linear_model = train_model(linear_model.LogisticRegression(), xtrain_tfidf_ngram, train_y, xvalid_tfidf_ngram)
print ("LR, N-Gram Vectors: ", Ngram_Linear_model.accuracy)


# In[5]:


# -----------------SVM on Ngram Level TF IDF Vectors-------------------

# SVM(Gaussian Kernel) on Ngram Level TF IDF Vectors
# The C parameter trades off correct classification of training examples against maximization 
# of the decision function’s margin. For larger values of C, a smaller margin will be accepted 
# if the decision function is better at classifying all training points correctly. 
# A lower C will encourage a larger margin, therefore a simpler decision function, at the cost of
# training accuracy. In other words``C`` behaves as a regularization parameter in the SVM.

# ----------------About Simulation--------------/
# run a sumulation and get the otimal C value for our dataset.
# during simulation::
# with this dataset highest rbf accuracy = 0.9177, C=630

# ----------Simulation start---------------/
# pre_Accuracy = 0.0
# for i in range (400,630,10):
#     accuracy = train_model(svm.SVC(kernel='rbf',C=i), xtrain_tfidf_ngram, train_y, xvalid_tfidf_ngram)
#     if (abs(pre_Accuracy-accuracy)<0.001) or (abs(accuracy-0.9177)<0.001):
#         print ("Optimal C value for Gaussian kernel:", i)
#         break
# ----------Simulation End---------------/
    
Ngram_SVM_RBF_model = train_model(svm.SVC(kernel='rbf',C=630.0), xtrain_tfidf_ngram, train_y, xvalid_tfidf_ngram)
print("SVM(Gaussian), N-Gram Vectors: ", Ngram_SVM_RBF_model.accuracy)

Ngram_SVM_Poly_model = train_model(svm.SVC(kernel='poly'), xtrain_tfidf_ngram, train_y, xvalid_tfidf_ngram)
print("SVM(Polynomial), N-Gram Vectors: ", Ngram_SVM_Poly_model.accuracy)


# In[6]:


# -------------RF-Bagging Model-----------------//

# Random Forest models are a type of ensemble models, particularly bagging models.
# They are part of the tree based model family.

# RF on Count Vectors
CountV_RF_model = train_model(ensemble.RandomForestClassifier(), xtrain_count, train_y, xvalid_count)
print("RF, Count Vectors: ", CountV_RF_model.accuracy)

# RF on Word Level TF IDF Vectors
CountV_RF_model = train_model(ensemble.RandomForestClassifier(), xtrain_tfidf, train_y, xvalid_tfidf)
print("RF, WordLevel TF-IDF: ", CountV_RF_model.accuracy)


# In[8]:


# ------------Voting--Bagging Model-----------------//
# Ensemble method for majority voting
Voting_Bagging_model = ensemble.VotingClassifier(estimators=[('ng_svm', Ngram_SVM_RBF_model), ('ng_linear', Ngram_Linear_model), ('ng_naive', Ngram_Naive_model)], voting='hard')
Voting_Bagging_model = Voting_Bagging_model.fit(xtrain_tfidf_ngram, train_y)
# predict the labels on validation dataset
accuracy = Voting_Bagging_model.score(xvalid_tfidf_ngram,valid_y)
# ensemble.score(X_test,Y_test)*100,"%"
print("Accuracy with Ensemble(majority voting) ", accuracy)

