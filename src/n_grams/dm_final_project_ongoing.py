
# coding: utf-8

# In[139]:


#Imports
import nltk, re, string, collections
from nltk.util import ngrams # function for making ngrams
import glob
import xml.etree.ElementTree as ET
from xml.etree.ElementTree import Element, SubElement, Comment, tostring, ElementTree
from lxml import etree


# In[140]:


#taking all the xmls of the directory as sorted
all_data_xmls = glob.glob("./data/articles/*.xml")
all_data_xmls.sort()
print(len(all_data_xmls))
#print(all_data_xmls)


# In[141]:


maintexts = []
esBigrams = []
esTrigrams = []
esFourgrams = []
for i in range(len(all_data_xmls)):
    esBigrams.append([])
    esTrigrams.append([])
    esFourgrams.append([])
print(esBigrams[0])


# In[142]:


for each_data_xml in all_data_xmls:
    tree = ET.parse(each_data_xml)
    root = tree.getroot()
    for child in root:
        if(child.tag == 'mainText'):
            maintexts.append(child.text)


# In[143]:


print(len(maintexts))
#print(maintexts[0:2])


# In[155]:


i=0
ne=0
for maintext in maintexts:
    if maintext:
        tokenized = maintext.split()
        ne+=1
        # and get a list of all the bi-grams, tri-grams, four-grams
        esBigrams[i] = ngrams(tokenized, 2)
        esTrigrams[i] = ngrams(tokenized, 3)
        esFourgrams[i] = ngrams(tokenized, 4)
        # get the frequency of each bigram, tri-gram, four-gram in our corpus
    i+=1
    
print ('Not empty maintexts: ',ne)


# In[156]:


for each_bigram in esBigrams:
    #output = list(each_bigram)
    #print(len(output))
    esBigramFreq = collections.Counter(each_bigram)
    print('5 most frequent bigrams: \n',esBigramFreq.most_common(5))
for each_trigram in esTrigrams:
    esTrigramFreq = collections.Counter(each_trigram)
    print('5 most frequent trigrams: \n',esTrigramFreq.most_common(5))
    
for each_fourgram in esFourgrams:
    esFourgramFreq = collections.Counter(each_fourgram)
    print('5 most frequent fourgrams: \n',esFourgramFreq.most_common(5))


# In[157]:


#print('5 most frequent bigrams: \n',esBigramFreq.most_common(5))
#print('5 most frequent trigrams: \n',esTrigramFreq.most_common(5))
#print('5 most frequent fourgrams: \n',esFourgramFreq.most_common(5))

