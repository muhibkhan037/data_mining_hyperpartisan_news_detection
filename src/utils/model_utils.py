from sklearn import metrics
from sklearn.pipeline import Pipeline


def train_model(classifier, feature_vector_train, label, feature_vector_valid, valid_y, is_neural_net=False):
    # fit the training dataset on the classifier
    classifier.fit(feature_vector_train, label)

    # predict the labels on validation dataset
    predictions = classifier.predict(feature_vector_valid)

    if is_neural_net:
        predictions = predictions.argmax(axis=-1)

    return metrics.accuracy_score(predictions, valid_y)


def train_and_score(classifier, classifier_name, feature_pipeline, train_x, train_y, valid_x, valid_y):

    classifier_pipeline = Pipeline([('feats', feature_pipeline),
                                    ('classifier', classifier)])
    print('Started training for', classifier_name)
    model = classifier_pipeline.fit(train_x, train_y)
    print('Training completed for', classifier_name)
    predicted = model.predict(valid_x)
    score = metrics.accuracy_score(predicted, valid_y)
    predict_proba = model.predict_proba(valid_x)
    return score, predict_proba
